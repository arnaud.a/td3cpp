#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
using namespace std;

#include <iostream>
#include <math.h>

using namespace std;
// Exo 1
class point
{
private:
    float x, y;
    float rho, theta;
    void majpol();
public:
    point();
    point(float);
    point(float,float);
    ~point();
    float getx() {return x;}
    float gety() {return y;}
    void affiche();
    void init();
    void init(float, float);
    void deplace(float, float);
    void homothetie1();
    void homothetie2(float);
    void rotation(float);
};

point::point()
{
    cout<<"constructeur1"<<endl;
    x=y=0;
    // cin >> x >> b;
    majpol();
}
point::point(float px)
{
    cout<<"constructeur2"<<endl;
    x=y=px;
    majpol();
}
point::point(float px, float py)
{
    cout<<"constructeur3"<<endl;
    x=px;
    y=py;
    majpol();
}

point::~point()
{
    cout<<"destructeur"<<endl;
}
void point::init()
{
    cout<<"Entrer x puis y" <<endl;
    cin >> x >> y;
    majpol();
}

void point::init(float nx, float ny)
{
    x=nx;
    y=ny;
    majpol();
}
void point::affiche()
{
    cout<<"x="<<x<<endl;
    cout<<"y="<<y<<endl;
    cout<<"rho="<<rho<<endl;
    cout<<"theta="<<theta<<endl;
}
void point::deplace(float dx, float dy)
{
    x=x+dx;
    y=y+dy;
    majpol();;
}

void point::homothetie1()
{
    int coef;
    cout<<"Entrer le coef multiplicateur"<<endl;
    cin >> coef ;
    x=x*coef;
    y=y*coef;
    majpol();;
}
void point::homothetie2(float coef)
{
    x=x*coef;
    y=y*coef;
    majpol();
}

void point::rotation(float angle)
{
    angle=angle*M_PI/180;
    //cin>>angle;
    theta=theta+angle;
    x=rho*cos(theta);
    y=rho*sin(theta);
}

void point::majpol()
{
    rho=sqrt((x*x)+(y*y));
    theta=atan(y/x);
}

class rectangle
{
    protected:
        point p1,p2;
    public:
        rectangle();
        rectangle(float,float,float,float);
        rectangle(point,float,float);
        rectangle(point,point);
        ~rectangle();
        void init(float,float,float,float);
        void affiche();
        bool selection(float,float);
        rectangle(const rectangle&);
        rectangle& operator=(const rectangle&);
};

rectangle::rectangle(): p1(), p2()
{}

rectangle::rectangle(float px1, float py1, float px2, float py2):p1(px1,py2),p2(px2,py2)
{}

rectangle::rectangle(point pp,float pl, float ph):p1(pp),p2(pp.getx()+pl , pp.getx()+ph)
{}
rectangle::rectangle(point pp1,point pp2):p1(pp1),p2(pp2)
{}

rectangle::~rectangle()
{}

rectangle::rectangle(const rectangle&s):p1(s.p1),p2(s.p2)
{}
/*
rectangle::rectangle(const rectangle&s)
{
    p1 = s.p1;
    p2 = s.p2;
}
*/
void rectangle::init(float px1, float py1, float px2, float py2)
{
    p1.init(px1,px2);
    p2.init(px2,py2);
}

void rectangle::affiche()
{
    cout << "objet rectangle - " << "adresse = " << this << endl;
    p1.affiche();
    p2.affiche();
    //rectfill(page,x1,y1,x2,y2)makecol(0,255,0);
}

bool rectangle::selection(float sx, float sy)
{
    if (sx>=p1.getx() && (sx <= p2.getx())&&(sy>=p1.gety()) && (sy <= p2.gety()))
        {return true;}
    return false;
}

class bouton
{
    protected:
        rectangle r;
        string ch;
    public:
        bouton();
        bouton(float,float,char *);
        bouton(point,point, char *);
        bouton(point,float,float);
        bouton(point,float,float,char*);
        bouton(float,float,float,float,char *);
        bouton(rectangle,char*);
        ~bouton();
        //bouton(const bouton&);
        //bouton&operator = (const bouton&);
        void init(float,float,char*);
        void affiche();
        bool selection (float sx,float sy){return r,selection(sx,sy);}
};
bouton::bouton():r(),ch()
{

}
bouton::bouton(float px, float py, char* pch): r(point(px,py),strlen(pch)+10,20)
,ch(pch)
{
}
bouton::bouton(float px1, float py1, float px2, float py2, char* pch)
{
}
bouton::bouton(point pp, float ph, float pl, char* pch)
{
}
bouton::bouton (point pp1, point pp2, char* pch)
{
}
bouton::bouton(rectangle pr, char* pch)
{
}
bouton::~bouton()
{
}
//bouton::bouton(const bouton &s):r(s,r),ch(s.ch){}
/* bouton & bouton :: operator=(const bouton&s)
{
    ch=s.ch;
    r=s.r;
    return *this;
}
*/
void bouton::init(float px, float py, char*pt)
{
    r.init(px,py,px+strlen(pt)+10,py+60);
    ch.append(pt);
}
void bouton::affiche()
{
    cout<<"Objet bouton - " << "adresse = "<<this<<endl;
    r.affiche();
    cout<<"longue chaine =" << ch.length()<<"et chaine =" << ch << endl;
}

class menu
{
    protected :
        bouton *tab;
        int n;
    public :
        menu(char*[],int);
        ~menu();
        menu(const menu &);
       menu& operator=(const menu&);
        void affiche();
        int selection (float,float);
};

menu::menu(char*t[], int pn)
{
    n=pn;
    tab = new bouton[n];
    for (int i = 0;i<n;i++)
        tab[i].init(10*i,5/i,t[i]);
}

menu::menu(const menu & s)
{
    n = s.n;
    tab = new bouton[n];
    for(int i = 0;i<n;i++)
        tab[i]=s.tab[i];
}
menu::~menu()
{
delete tab;
}

menu& menu::operator=(const menu&s)
{
    if (this != &s)
    {
        delete []tab;
        n=s.n;
        tab=new bouton[n];
        for (int i=0;i<n;i++)
            tab[i]=s.tab[i];
    }
}

void menu::affiche()
{
    cout << "menu : " << endl;
    for (int i=0;i<n;i++)
        tab[i].affiche();
}

int menu::selection(float px, float py)
{
    for (int i=0; i<n;i++)
    {
        if(tab[i].selection(px,py)==true) return i;
        return -1;
    }
}
class menubis
{
protected:
        bouton **tab;
        int n;
public:
        menubis(char*[],int);
        ~menubis();
        menubis(const menubis &);
        menubis& operator=(const menubis&);
        void affiche();
        int selection (float,float);
        bool selection (float sx,float sy){return r,selection(sx,sy);}

};

menubis::menubis(char *t[],int pn)
{
    n=pn;
    tab = new bouton*[n];
    for (int i = 0;i<n;i++)
        tab[i] = new bouton(10*i,5*i,t[i]);
}
menubis::~menubis()
{
    for (int i=0;i<n;i++)
        delete tab[i];
    delete tab;
}

/*menubis::menubis(const menubis& s)
{
n = s.n;

for (int i=0;i<n;i++)
}  */

void menubis::affiche()
{
    cout<<"menusbis:"<<endl;
    for (int i=0;i<n;i++)
        tab[i]->affiche();
}

int menubis::selection(float px, float py)
{
    for (int i = 0 ;i<n;i++)
    {
        if(tab[i]->selection(px,py)== true) return i;
    }
    return -1;
}

class menuter
{
    protected:
        vector <bouton> *tab;
        int n;
    public:
        menuter(char*[], int);
        ~menuter();
        menuter(const menuter&);
        menuter& operator=(const menuter&);
        void affiche();
        bool selection(int,int) {return false;}
};

menuter::menuter(char *t[], int pn)
{
    n=pn;
    tab=new vector<bouton>; //<bouton*>
    for (int i=0; i<n;i++)
    tab->push_back(bouton(10*i,5*i,t[i])); // new bouton (...)
}

menuter::~menuter()
{
   delete tab;
}

menuter::menuter(const menuter &s)
{
    n=s.n;
    tab=new vector <bouton>(n);
    for (int i=0;i<n;i++)
        (*tab)[i]=(*s.tab)[i];
}

menuter& menuter::operator=(const menuter&s)
{
    if (this!=&s)
    {
        delete tab;
        n=s.n;
        tab = new vector <bouton>(n);
        for (int i=0;i<n;i++)
        {
            (*tab)[i]=(*s.tab)[i];
        }
    }
    return *this;
}

void menuter::affiche()
{
    cout<<"menuter :" << endl;
    for (int i=0;i<n;i++){
        (*tab)[i]=(*s.tab)[i];
    }
}

int menuter::selection(float px, float py)
{
    for (int i=0;i<n;i++)
    {
        if ((*tab)[i].selection(px,py)==true) return i;
        return -1;
    }
}
int main()
{
    point p(10,20); p.affiche();
    rectangle1()

    cout<<endl<<"******* MENU 1 *******" << endl;
    menu m1(t,4);
    m1.affiche();
    menu m2 (t,2);
    m2.affiche();
    m2=m1;
    m2.affiche();

    cout<<endl<<"******* MENU 2 *******" << endl;
    menusbis m21(t,4);
    m21.affiche();
    menubis m22(t,2);
    m22.affiche();
    cout<<endl<<"******* MENU 3 *******" << endl;
    menuter m3(t,4);m3.affiche();
    menuter m4(m3); m4.affiche();
    menuter m5(t,2);
    m5=m3;m5.affiche();
    cout << "Hello world!" << endl;
    return 0;
}

