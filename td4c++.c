#include <iostream>

using namespace std;

#include <iostream>
#include <math.h>

using namespace std;
// Exo 1
class point
{
private:
    float x, y;
    float rho, theta;
    void majpol();
public:
    point();
    point(float);
    point(float,float);
    ~point();
    void affiche();
    void initialise1();
    void initialise2(float, float);
    void deplace(float, float);
    void homothetie1();
    void homothetie2(float);
    void rotation(float);
};

point::point()
{
    cout<<"constructeur1"<<endl;
    x=y=0;
    // cin >> x >> b;
    majpol();
}
point::point(float px)
{
    cout<<"constructeur2"<<endl;
    x=y=px;
    majpol();
}
point::point(float px, float py)
{
    cout<<"constructeur3"<<endl;
    x=px;
    y=py;
    majpol();
}

point::~point()
{
    cout<<"destructeur"<<endl;
}
void point::initialise1()
{
    cout<<"Entrer x puis y" <<endl;
    cin >> x >> y;
    majpol();
}

void point::initialise2(float nx, float ny)
{
    x=nx;
    y=ny;
    majpol();
}
void point::affiche()
{
    cout<<"x="<<x<<endl;
    cout<<"y="<<y<<endl;
    cout<<"rho="<<rho<<endl;
    cout<<"theta="<<theta<<endl;
}
void point::deplace(float dx, float dy)
{
    x=x+dx;
    y=y+dy;
    majpol();;
}

void point::homothetie1()
{
    int coef;
    cout<<"Entrer le coef multiplicateur"<<endl;
    cin >> coef ;
    x=x*coef;
    y=y*coef;
    majpol();;
}
void point::homothetie2(float coef)
{
    x=x*coef;
    y=y*coef;
    majpol();
}

void point::rotation(float angle)
{
    angle=angle*M_PI/180;
    //cin>>angle;
    theta=theta+angle;
    x=rho*cos(theta);
    y=rho*sin(theta);
}

void point::majpol()
{
    rho=sqrt((x*x)+(y*y));
    theta=atan(y/x);
}

class rectangle
{
    protected:
        point p1,p2;
    public:
        rectangle();
        rectangle(float,float,float,float);
        rectangle(point,float,float);
        rectangle(point,point);
        ~rectangle();
        void init(float,float,float,float);
        void affiche();
        bool selection(float,float);
        rectangle(const rectangle&);
        rectangle& operator=(const rectangle&);
};

rectangle::rectangle(): p1(), p2()
{}

rectangle::rectangle(float px1, float py1, float px2, float py2):p1(px1,py2),p2(px2,py2)
{}

rectangle::rectangle(point pp,float pl, float ph):p1(pp),p2(pp.getx()+pl , pp.getx()+ph)
{}
rectangle::rectangle(point pp1,point pp2):p1(pp1),p2(pp2)
{}

rectangle::~rectangle()
{}

rectangle::rectangle(const rectangle&s):p1(s.p1),p2(s.p2)
{}
rectangle::rectangle(const rectangle&s)
{
    p1 = s.p1;
    p2 = s.p2;
}

void rectangle::init(float px1, float py1, float px2, float py2)
{
    p1.init(px1,px2);
    p2.init(px2,py2);
}

void rectangle::affiche()
{
    cout << "objet rectangle - " << "adresse = " << this << endl;
    p1.affiche();
    p2.affiche();
    //rectfill(page,x1,y1,x2,y2)makecol(0,255,0);
}

bool rectangle::selection(float sx float sy)
{
    if (sw>=p1.getx()) && (sx <= p2.getx())&&(sy>=p1.gety()) && (sy <= p2.gety())
        {return true;}
    return false;
}

class bouton
{
    protected
        retangle r;
        string ch;
    public:
        bouton();
        bouton(float,float,char *);
        bouton(point,point, char *);
        bouton(point,float,float);
        bouton(point,float,float,char*);
        bouton(float,float,float,float,char *);
        bouton(rectangle,char*);
        ~bouton();
        //bouton(const bouton&);
        //bouton&operator = (const bouton&);
        void init(float,float,char*);
        void affiche();
        bool selection (float sx,float sy){return r,selection(sx,sy);}
};
bouton::bouton():r(),ch()
{

}
bouton::bouton(float px, float py, char* pch): r(point(px,py)).strlen(pch)+10,20)
,ch(pch)
{
}
bouton::bouton(float px1, float py1, float px2, float py2, char* pch)
{
}
bouton::bouton(point pp, float ph, float pl, char* pch)
{
}
bouton::bouton (point pp1, point pp2, char* pch)
{
}
boutton::bouton(rectangle pr, char* pch)
{
}
bouton::~bouton()
{
}
//bouton::bouton(const bouton &s):r(s,r),ch(s.ch){}
/* bouton & bouton :: operator=(const bouton&s)
{
    ch=s.ch;
    r=s.r;
    return *this;
}
*/
void bouton::init(float px; float py, char*pt)
{
    r.init(px,py,px+strlen(pt)+10,py+60);
    ch.appen(pt);
}
void bouton::affiche()
{
    cout<<"Objet bouton - " << "adresse = "<<this<<endl;
    r.affiche();
    cout<<"longue chaine =" << ch.length()<<"et chaine =" << ch << endl;
}


